﻿using UnityEngine;
using System.Collections;

public abstract class ObjectPoolDemo_Object_Base : MonoBehaviour
{

    /// <summary>
    /// using for initialization of things that need to be done before they are added to the pool
    /// </summary>
    protected virtual void Start()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// this is where we will do the runtime instantiation of the object for when it is used in the pool
    /// </summary>
    protected virtual void OnEnable()
    {

    }

    /// <summary>
    /// this is where we will do any clean up from the object i.e. shutting of attached childeren or particles
    /// </summary>
    protected virtual void OnDisable()
    {
        
    }


}
