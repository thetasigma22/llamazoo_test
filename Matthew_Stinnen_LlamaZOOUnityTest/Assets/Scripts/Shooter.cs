﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

    ObjectPoolDemo_Manager poolManager;
    public float turnSpeed = 40.0f;
    public float MoveSpeed = 10.0f;
    public float shotDelay = 0.25f;
    float shotTimer = 0.0f;
	// Use this for initialization
	void Start ()
    {
        poolManager = GetComponent<ObjectPoolDemo_Manager>();
	}
	
	// Update is called once per frame
	void FixedUpdate()
    {
        shotTimer += Time.fixedDeltaTime; 
        if (shotTimer >= shotDelay)
        {
            shotTimer = 0.0f;
            if (Input.GetKey(KeyCode.Space))
            {
                ObjectPoolDemo_Object_BulletTest bullet = (ObjectPoolDemo_Object_BulletTest)poolManager.GetObject();
                if (bullet)
                {
                    bullet.Shooter = gameObject;
                    bullet.transform.position = transform.position;
                    bullet.transform.forward = transform.forward;
                    bullet.gameObject.SetActive(true);
                }

            }
        }
        if(Input.GetKey(KeyCode.A))// turn left
        {
            transform.Rotate(transform.up, Time.fixedDeltaTime * -turnSpeed);
        }
        if (Input.GetKey(KeyCode.D))// turn right
        {
            transform.Rotate(transform.up, Time.fixedDeltaTime * turnSpeed);
        }
        if (Input.GetKey(KeyCode.W))// Move
        {
            transform.position = transform.position + (transform.forward * Time.fixedDeltaTime * MoveSpeed);
        }
    }
}
