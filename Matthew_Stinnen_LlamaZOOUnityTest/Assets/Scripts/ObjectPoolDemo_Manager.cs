﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolDemo_Manager : MonoBehaviour
{
    public ObjectPoolDemo_Object_Base pooledObject;
    public int poolSize = 10;
    public bool expandPool = false;

    List<ObjectPoolDemo_Object_Base> objectPool;

	// Use this for initialization
	void Start ()
    {
        objectPool = new List<ObjectPoolDemo_Object_Base>();

        for (int i = 0; i < poolSize; ++i)
        {
           objectPool.Add(Instantiate(pooledObject));


        }
	}

    /// <summary>
    /// this will return an object that is available to use, you still need to be in charge of activating and deactivating it as those conditions are on a per use basis
    /// </summary>
    /// <returns>an object that is avalable from the pool, or null if there is none</returns>
    public ObjectPoolDemo_Object_Base GetObject()
    {
        for (int i = 0; i < objectPool.Count; ++i)
        {
            if (objectPool[i].isActiveAndEnabled == false) // we only want to return objects that are not active as active objects would most likely be still being used/ viewed 
            {
                return objectPool[i];
            }
        }

        if (expandPool) // if we want a dynamic pool, we can add extra ones if we run out of space, will act as a normal pool when it has enough objects
        {
            objectPool.Add(Instantiate(pooledObject));
        }

        return null; 
    }
}
