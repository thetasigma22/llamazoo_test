﻿using UnityEngine;
using System.Collections;

public class ObjectPoolDemo_Object_BulletTest : ObjectPoolDemo_Object_Base {

    public float speed = 10.0f;
    public float maxDistFromShooter = 20.0f;
    public GameObject Shooter;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
    }

    protected override void Start()
    {
        base.Start();
    }

    void FixedUpdate()
    {
        transform.position = transform.position + (transform.forward * Time.fixedDeltaTime * speed);
        if(Vector3.Distance(transform.position,Shooter.transform.position) > maxDistFromShooter) // for lack of a better quick way of doing this, i am allowing a max distance from the player to be used
        {
            gameObject.SetActive(false);
        }
    }
}
